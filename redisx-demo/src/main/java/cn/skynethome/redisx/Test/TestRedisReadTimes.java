package cn.skynethome.redisx.Test;

import cn.skynethome.redisx.RedisSharedMasterSlaveSentinelUtil;

public class TestRedisReadTimes
{
    public static void main(String[] args)
    {
        RedisSharedMasterSlaveSentinelUtil.setString("aaaabbbb", "fsdfsdfasdgdfgdsfgdgfd");

        for (int i = 0; i < 2000; i++)
        {
            new TimeRedis().start();
        }

    }
}

class TimeRedis extends Thread
{
    @Override
    public void run()
    {
        long start = System.currentTimeMillis();
        String str = RedisSharedMasterSlaveSentinelUtil.getString("aaaabbbb");
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }
}